/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kaewmanee;

import java.io.Serializable;

/**
 *
 * @author Acer
 */
public class myList implements Serializable{
    private String detail;
    private String type;
    private int money;
    private String note;
    private int year;    
    private String mount;
    private String day;
    public myList(String detail,String type,int money,String note,int year,String mount,String day){
        this.detail = detail;
        this.type = type;
        this.detail = detail;
        this.note = note;
        this.money = money;
        this.day = day;
        this.mount = mount;
        this.year = year;
    }
    public String getDetail(){
        return detail;
    }
    public String getType(){
        return type;
    }
    public Integer getMoney(){
        return money;
    }
    public String getNote(){
        return note;
    }
    public int getYear(){
        return year;
    }
    public String getMount(){
        return mount;
    }
    public String getDay(){
        return day;
    }
    
    public void setDetail(String detail){
         this.detail = detail; 
    }
    public void setType(String type){
        this.type = type;
    }
    public void setMoney(int money){
        this.money = money;
    }
    public void setNote(String note){
        this.note = note;
    }
    public void setYear(int year){
        this.year = year;
    }
    public void setMount(String mount){
        this.mount = mount;
    }
    public void setDay(String day){
        this.day = day;
    }
    @Override
    public String toString() {
        return " Date: " + year + " - " + mount + " - " + day + " || detail= " + detail + " || " + type + " => " + money + " | note : " + note   ;
    }
}
