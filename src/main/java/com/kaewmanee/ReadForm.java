/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kaewmanee;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class ReadForm {
     public static void main(String[] args) {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        
        try{
           file = new File("list.bin");
           fis = new FileInputStream(file);
           ois = new ObjectInputStream(fis);
           ArrayList<myList> myListlist = (ArrayList<myList>)ois.readObject();
           System.out.println(myListlist);
           fis.close();
           
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadForm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReadForm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ReadForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
