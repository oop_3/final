/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kaewmanee;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class WriteForm {
     public static void main(String[] args) {
        ArrayList<myList> lists = new ArrayList();
        lists.add(new myList("mother ","income",500,"- ",2022,"January","3"));
        lists.add(new myList("father ","income",200,"- ",2022,"January","4"));
        lists.add(new myList("brother ","income",400,"- ",2022,"January","5"));
        lists.add(new myList("sister ","income",1000,"- ",2022,"January","6"));
        
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        
        try{
            file = new File("list.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(lists);
            oos.close();
            fos.close();
        }catch(FileNotFoundException ex){
            Logger.getLogger(WriteForm.class.getName()).log(Level.SEVERE,null,ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteForm.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        
        
        
    }
    
}
